# Reconhecimento de Gestos Utilizando Cadeias Ocultas de Markov 

Este projeto é o controle de versão da minha monografia do curso de graduação em
Ciência da Computação da Universidade Estadual do Norte Fluminense (UENF).

# Dependências

Para poder compilar, algumas dependências devem ser instaladas no seu sistema.
As dependências são baseadas na distribuição Linux Ubuntu.

    sudo apt-get install texlive-latex-base abntex texlive-fonts-recommended texlive-lang-portuguese

    sudo easy_install pygments pygments-rspec pygments-style-github

# Compilando

Para compilar, depois ter instalado as dependências, basta rodar o comando
`make` ou `make pdf`.

